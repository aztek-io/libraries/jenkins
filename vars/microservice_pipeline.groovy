/* groovylint-disable DuplicateStringLiteral, ImplicitClosureParameter, MethodSize */

def call() {
    pipeline {
        agent {
            kubernetes {
                yaml microservice_pod_def()
            }
        }

        options {
            timeout(time: 30, unit: 'MINUTES')
        }

        stages {
            stage('Init') {
                steps {
                    script {
                        init()
                        config = getProjectConfig()

                        if (config?.docker?.repo?.image) {
                            image = config.docker.repo.image
                        } else {
                            image = 'foo/bar'
                        }

                        if (fileExists('version.yaml')) {
                            version = readYaml('version.yaml').version
                        } else {
                            version = 'v0.1.0-beta.1'
                        }
                    }
                }
            }

            stage('Lint') {
                parallel {
                    stage('Lint Groovy files') {
                        when {
                            anyOf {
                                expression {
                                    microservice_pod_def.checkPatterns(microservice_pod_def.groovyLintFilePatterns())
                                }
                            }
                        }

                        steps {
                            script {
                                lint.groovylint()
                            }
                        }
                    }

                    stage('Lint Dockerfile') {
                        when {
                            anyOf {
                                expression {
                                    microservice_pod_def.checkPatterns(microservice_pod_def.dockerLintFilePatterns())
                                }
                            }
                        }

                        steps {
                            script {
                                lint.hadolint()
                            }
                        }
                    }

                    stage('Lint shell scripts') {
                        when {
                            anyOf {
                                expression {
                                    microservice_pod_def.checkPatterns(microservice_pod_def.shellcheckFilePatterns())
                                }
                            }
                        }

                        steps {
                            script {
                                lint.shellcheck()
                            }
                        }
                    }
                }
            }

            stage('Build') {
                parallel {
                    stage('Docker build') {
                        when {
                            allOf {
                                expression {
                                    microservice_pod_def.checkPatterns(microservice_pod_def.dockerBuildFilePatterns())
                                }
                                expression { fileExists('Dockerfile') }
                            }
                        }

                        steps {
                            script {
                                build.docker(
                                    image: "${image}",
                                    version: "${version}",
                                )
                            }
                        }
                    }
                }
            }

            /*
            stage('Test') {
                steps {
                    println('Placeholder')
                }
            }
            */

            stage('Quality') {
                parallel {
                    stage('Trivy Scan') {
                        when {
                            allOf {
                                expression {
                                    microservice_pod_def.checkPatterns(microservice_pod_def.dockerBuildFilePatterns())
                                }
                                expression { fileExists('Dockerfile') }
                            }
                        }

                        steps {
                            script {
                                quality.trivy(
                                    image: "${image}",
                                    version: "${version}",
                                )
                            }
                        }
                    }
                }
            }

            /*
            stage('Push Artifacts') {
                steps {
                    println('Placeholder')
                }
            }
            */

            stage('Finalize') {
                steps {
                    last()
                }
            }
        }
    }
}
