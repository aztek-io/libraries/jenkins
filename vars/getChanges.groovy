/* groovylint-disable NestedForLoop */
@NonCPS
def changes() {
    changedFiles = []

    for (changeSet in currentBuild.changeSets) {
        for (commit in changeSet.items) {
            for (file in commit.getPaths()) {
                changedFiles.add(file.getPath())
            }
        }
    }

    uniqueFiles = changedFiles.unique()

    return uniqueFiles
}

def call() {
    changes()
}
