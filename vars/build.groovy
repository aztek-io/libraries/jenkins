def docker(Map parameters) {
    image   = parameters.image
    version = parameters.version

    script {
        container('dind') {
            sh """
                export DOCKER_BUILDKIT=1

                docker build \
                    -t "${image}:${version}" \
                    .
            """
        }
    }
}
