/* groovylint-disable DuplicateStringLiteral, ImplicitClosureParameter */
def pod() {
    return '''
apiVersion: v1
kind: Pod
metadata:
  labels:
    type: ephemeral-jenkins-agent
    pipeline: generic_pipeline
spec:
  containers:
<% if (groovylint) {%>\
  - name: groovylint
    image: 'nvuillam/npm-groovy-lint:8.1.0'
    command:
    - cat
    tty: true
<% } %>\
<% if (hadolint) {%>\
  - name: hadolint
    image: 'hadolint/hadolint:v1.19.0-45-gef91156-debian'
    command:
    - cat
    tty: true
<% } %>\
<% if (shellcheck) {%>\
  - name: shellcheck
    image: 'koalaman/shellcheck-alpine:v0.7.1'
    command:
    - cat
    tty: true
<% } %>\
<% if (docker_build) {%>\
  - name: dind
    image: 'docker:20.10.2-dind'
    command:
    - cat
    tty: true
    volumeMounts:
    - name: docker-socket
      mountPath: /var/run/docker.sock
    - name: trivy-cache
      mountPath: /tmp/trivy/
<% } %>\
<% if (docker_build) {%>\
  volumes:
  - name: docker-socket
    hostPath:
      path: /var/run/docker.sock
  - name: trivy-cache
    hostPath:
      path: /tmp/trivy/
<% } else { %>\
  volumes: []
<% } %>\
'''
}

def checkPatterns(method) {
    return getChanges().any { it =~ /${method.join('|')}\$/ } || pullRequest()
}

def affectAllPatterns() {
    return [
        'Jenkinsfile',
        'jenkins.yaml',
    ]
}

def groovyLintFilePatterns() {
    return [
        '.*.groovy'
    ] + affectAllPatterns()
}

def dockerLintFilePatterns() {
    return [
        'Dockerfile'
    ] + affectAllPatterns()
}

def shellcheckFilePatterns() {
    return [
        '.*.sh',
    ] + affectAllPatterns()
}

def dockerBuildFilePatterns() {
    return [
        '.*Dockerfile',
        '.*.json',
        '.*.groovy',
        '.*.sln',
        '.*.cs',
        '.*.go',
        '.*.mod',
        '.*.py',
        '.*.sh',
    ] + affectAllPatterns()
}

def pullRequest() {
    return (env.gitlabActionType == 'MERGE' || env.bitbucketPlaceholder == 'PR' || env.gitHubPlaceholder == 'PR')
}

def call() {
    def binding = [
        [
            groovylint: checkPatterns(groovyLintFilePatterns()),
            hadolint: checkPatterns(dockerLintFilePatterns()),
            shellcheck: checkPatterns(shellcheckFilePatterns()),
            docker_build: checkPatterns(dockerBuildFilePatterns()),
        ]
    ]

    def engine = new groovy.text.SimpleTemplateEngine()
    def template = engine.createTemplate(pod()).make(binding).toString()

    return template
}
