def trivy(Map parameters) {
    image = parameters.image
    tag   = parameters.tag

    script {
        container('dind') {
            sh """
                docker run \
                    --rm \
                    --network="host" \
                    -v /var/run/docker.sock:/var/run/docker.sock \
                    -v /tmp/trivy:/root/.cache/ \
                    aquasec/trivy \
                        --exit-code=0 \
                        --severity MEDIUM \
                        ${image}:${tag}

                docker run \
                    --rm \
                    --network="host" \
                    -v /var/run/docker.sock:/var/run/docker.sock \
                    -v /tmp/trivy:/root/.cache/ \
                    aquasec/trivy \
                        --exit-code=1 \
                        --severity HIGH,CRITICAL \
                        ${image}:${tag}
            """
        }
    }
}
