def shellcheck() {
    script {
        container('shellcheck') {
            sh '''
            find . \
                -name '*.sh' \
                -print \
                -exec shellcheck "{}" ";"
            '''
        }
    }
}

def groovylint() {
    script {
        container('groovylint') {
            sh '''
            npm-groovy-lint \
                --failon warning \
                --verbose
            '''
        }
    }
}

def hadolint() {
    script {
        container('hadolint') {
            sh '''
            find . \
                -name Dockerfile \
                -exec hadolint "{}" ";"
            '''
        }
    }
}
