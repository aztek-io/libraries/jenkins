def call(Map parameters) {
    config = null

    if (parameters?.path) {
        configPath = parameters.path
    } else {
        configPath = 'jenkins.yaml'
    }

    if (fileExists(configPath)) {
        config = readYaml(file: configPath)
    } else {
        sh("echo 'Project config path does not exist: ${configPath}' && exit 1")
    }

    return config
}
