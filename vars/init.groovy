/* groovylint-disable DuplicateNumberLiteral, NestedForLoop */

def call() {
    script {
        println('Unique files changed in this change set: ' + getChanges())
        println('Pod Definition: \n' + microservice_pod_def())
        println(
            'Environment variables: \n' + sh(
                script: 'env',
                returnStdout: true
            )
        )
    }
}
