# docker build -t jenkins-installer . && docker run -it -v "$HOME/.aws/:/root/.aws/" -v "$HOME/.kube/:/root/.kube" jenkins-installer diff
# docker run -it -v "$HOME/.aws/:/root/.aws/" -v "$HOME/.kube/:/root/.kube" jenkins-installer sync

FROM aztek/helm:v0.1.0

ENV DOMAIN=jenkins.aztek.io

COPY ["helmfile.yaml", "."]

ENTRYPOINT ["/bin/helmfile", "-f", "helmfile.yaml"]
CMD ["diff"]
