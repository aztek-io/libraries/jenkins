# jenkins-libs

Example Jenkinsfile:

``` groovy
@Library('jenkins-libs@master') _
microservice_pipeline()
```

## Creating a new pipeline

You can easily create a copy of this pipeline (`vars/microservice_pipeline.groovy`) and call the new pipeline name in your Jenkinsfile.

## Installing jenkins in K8s

1. Build the dockerfile
  ```bash
  docker build -t jenkins-installer .
  ```

2. Generate a [helm diff](https://github.com/databus23/helm-diff)
  ```bash
  docker run -it \
      -v "$HOME/.aws/:/root/.aws/" \
      -v "$HOME/.kube/:/root/.kube/" \
      jenkins-installer diff
  ```
  **note:** the helm diff plugin was installed in the base container.

3. Sync the helmfile if the helm diff looks good.
  ```bash
  docker run -it \
      -v "$HOME/.aws/:/root/.aws/" \
      -v "$HOME/.kube/:/root/.kube/" \
      jenkins-installer sync
  ```

## Contributing

If contributing please make an effort to be idempotent by using when statements for file patterns.
